import React, { useState } from 'react'
import { Button } from 'antd'
import { PlayCircleOutlined } from '@ant-design/icons'

interface CompVideoProps {
  link?: string
  linkVideo: string
  styleVideo?:string
}

function Video(props: CompVideoProps) {
  const { link, linkVideo, styleVideo } = props
  const [visible, setVisible] = useState(false)

  const showModal = () => {
    setVisible(true)
  }

  const hideModal = () => {
    setVisible(false)
  }

  return (
    <div className="flex relative justify-center my-5 mt-[-110px]">
      <div className=" absolute z-2">
        <iframe
          src={link}
          className={styleVideo}
          title="Video"
          />
      </div>
      <div>
        <Button
          onClick={showModal}
          className="text-white font-bold rounded w-[100%] border-none mt-[100px] ml-[-80px]"
        >
          <PlayCircleOutlined className="text-9xl" />
        </Button>
        {visible && (
          <div className="fixed inset-0 flex items-center justify-center z-[99999] bg-black bg-opacity-80">
            <div className="bg-none rounded p-8 relative w-[1050px] h-[700px]">
              <span
                onClick={hideModal}
                className="absolute top-0 right-0 py-3 px-5 text-white text-[30px] md:top-[10px] cursor-pointer"
              >
                &times;
              </span>
              <iframe
                id="video-iframe"
                src={linkVideo}
                className="w-[100%] h-[550px] mt-8 rounded-[17px]"
                title="Video Player"
                frameBorder="0"
                allowFullScreen
              />
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default Video
